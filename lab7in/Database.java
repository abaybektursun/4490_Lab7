import java.util.*;
import java.sql.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
//import java.util.Properties;

public class Database
{
    private Properties  connectionString = new Properties();
	private InputStream inStream         = null;

    private String user;
    private String password;
    private String url;   
    
    private String query; 
 
    private Connection conn;
    
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 
    
    //Add any other data fields you like � at least a Connection object is mandatory
    public Database(String args[])
    {
        connectionString = new Properties();

        try {    
            inStream = new FileInputStream("lab7in/db.properties");

            connectionString.load(inStream);

            user     = connectionString.getProperty("user");
            password = connectionString.getProperty("password");
            url      = connectionString.getProperty("url");

        }catch (IOException ex){ex.printStackTrace();}
        finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        query = args[0];
        
        switch(args[1]){
            case "Q":
                    query(query);
                break;
            case "D":
                executeDML(query);  
        }
    }
    
    public ArrayList<String> query(String query)
    {
        Class.forName(JDBC_DRIVER) ;
        conn                   = DriverManager.getConnection(url, user, password);
        Statement stmt         = conn.createStatement();
        ResultSet rs           = stmt.executeQuery(query);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber      = rsmd.getColumnCount();
        
        ArrayList<String> result = new ArrayList<String>();
        while (rs.next()) {
            for(int i = 1; i < columnsNumber; i++)
                result
                System.out.print(rs.getString(i) + " ");
        }
        
        if (result.size() > 0)
            return result;
        else 
            return null;
    }
    
    public boolean executeDML(String dml)
    {
        Class.forName(JDBC_DRIVER) ;
        
        conn           = DriverManager.getConnection(url, user, password);
        Statement stmt = conn.createStatement();

        ResultSet rs;
        
        try{
            rs  = stmt.executeQuery(query);
            System.out.println("Success");
            return true;
        }catch (IOException ex){
            ex.printStackTrace();
            return false;
        }
    }
  
}
